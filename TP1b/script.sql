/* CREATE TYPES */

CREATE OR REPLACE TYPE Phone_List_Type AS VARRAY(3) of VARCHAR2(15);
/

CREATE OR REPLACE TYPE Category_Type IS TABLE OF VARCHAR(10);
/

CREATE OR REPLACE TYPE Country_Type AS OBJECT (
    CountryId VARCHAR2(30),
    CountryName VARCHAR2(30)
)

/

CREATE OR REPLACE TYPE City_Type AS OBJECT (
    CityId VARCHAR2(30),
    CityName VARCHAR2(30),
    CountryId VARCHAR2(30)
)

/

CREATE OR REPLACE TYPE PointofInterest_Type AS OBJECT (
    PoIId VARCHAR2(30),
    Name VARCHAR2(50),
    Description VARCHAR2(2000),
    CityId VARCHAR2(30),
    Category Category_Type
)

/

CREATE OR REPLACE TYPE Customer_Type AS OBJECT (  
   CustomerId  VARCHAR2(30),
   CustomerName VARCHAR2(30),
   UserEmail VARCHAR2(30),
   UserPassword VARCHAR2(30),
   CountryId VARCHAR2(30),
   Phone Phone_List_Type
)

/

CREATE OR REPLACE TYPE Score_Type AS OBJECT (
    ScoreId VARCHAR2(30),
    CustomerId VARCHAR2(30),
    PoIId VARCHAR2(30),
    Score DECIMAL,
    Comments VARCHAR2(2000)
)

/

/* INHERITANCE */


ALTER TYPE Customer_Type NOT FINAL CASCADE;
/

CREATE OR REPLACE TYPE PrivateCustomer_Type UNDER Customer_Type(
    Birthdate DATE,
    JobTitle VARCHAR2(50),
    Gender VARCHAR2(30)

);

/

CREATE OR REPLACE TYPE OrganizationCustomer_Type UNDER Customer_Type(
    Website VARCHAR2(100),
    Industry VARCHAR2(100),
    NIF VARCHAR2(9)
);

/



/* CREATE TABLES */

CREATE TABLE Country of Country_Type (
    CONSTRAINT pkCountry PRIMARY KEY(CountryId)
)
/

CREATE TABLE City of City_Type (
    CONSTRAINT pkCity PRIMARY KEY (CityId),
    CONSTRAINT fkCityCountry FOREIGN KEY (CountryId) REFERENCES Country
)
/

CREATE TABLE PointOfInterest of PointofInterest_Type (
    CONSTRAINT pkPoI PRIMARY KEY(PoIId),
    CONSTRAINT fkPoICity FOREIGN KEY (CityId) REFERENCES City   
  
)
NESTED TABLE Category STORE AS CategoryTable;

/

CREATE TABLE Customer of Customer_Type (
    CONSTRAINT pkCustomer PRIMARY KEY (CustomerId),
    CONSTRAINT fkCustomerCountry FOREIGN KEY (CountryId) REFERENCES Country
)
/

CREATE TABLE Score of Score_Type (
    CONSTRAINT pkScore PRIMARY KEY (ScoreId),
    CONSTRAINT fkScoreCustomer FOREIGN KEY (CustomerId) REFERENCES Customer,
    CONSTRAINT fkScorePoI FOREIGN KEY (PoIId) REFERENCES PointOfInterest 
)
/


CREATE TABLE PrivateCustomer of PrivateCustomer_Type
(
    CONSTRAINT pkPrivatecustomer PRIMARY KEY (CustomerId)
)

/

CREATE TABLE OrganizationCustomer of OrganizationCustomer_Type
(
    CONSTRAINT pkOrganizationCustomer PRIMARY KEY (CustomerId)
)

/



/* INSERT DATA */

INSERT INTO Country VALUES(1, 'Portugal');
INSERT INTO Country VALUES(2, 'United States');
INSERT INTO Country VALUES(3, 'Switzerland');

/


INSERT INTO City VALUES(1, 'Viseu', 1);
INSERT INTO City VALUES(2, 'Porto', 1);
INSERT INTO City VALUES(3, 'New York', 2);
INSERT INTO City VALUES(4, 'Zurich', 3);

/


INSERT INTO PointOfInterest VALUES(1, 'Wooden Tower', 'Wooden Tower located in Wil', 4, Category_Type('Rural'));
INSERT INTO PointOfInterest VALUES(2, 'Central Park', 'Central Parks amazing view ', 3, Category_Type('Urban'));
INSERT INTO PointOfInterest VALUES(3, 'Dragon Stadium', 'Host to FC Porto football club', 2, Category_Type('Urban'));
INSERT INTO PointOfInterest VALUES(4, 'Wil Lake', 'Great location in the summer', 4, Category_Type('Water'));
INSERT INTO PointOfInterest VALUES(5, 'Fontelo', 'Relaxing, camping site', 1, Category_Type('Forest'));

/


INSERT INTO Customer VALUES(1, 'Joao Bastos', 'bastosj', 'Viseu2014', 1, Phone_List_Type('939650426', '910773884'));
INSERT INTO Customer VALUES(2, 'Julio Rodrigues', 'jrodrigues', 'jmaxfun', 1, Phone_List_Type('939650377', '960574812'));

/

INSERT INTO OrganizationCustomer VALUES(3, 'Bizdirect', 'bizdirect', 'bizdirect2014', 2, Phone_List_Type('213817635'), 'https://www.bizdirect.pt', 'IT', '505046555');
INSERT INTO OrganizationCustomer VALUES(4, 'Microsoft', 'msft', 'msft2014', 2, Phone_List_Type('211876109'), 'https://www.microsoft.com', 'IT', '501867103');
INSERT INTO PrivateCustomer VALUES(5, 'Joao Fernandes', 'fernandesj', 'oBastosEhOMaior', 2, Phone_List_Type('916671908'), TO_DATE('1991/04/07', 'yyyy/mm/dd') , 'Melhor Designer do Mundo', 'MALE');
INSERT INTO PrivateCustomer VALUES(6, 'Toze Dias', 'diastoze', 'oBastosEhOMaior2', 2, Phone_List_Type('934691958'), TO_DATE('1991/06/13', 'yyyy/mm/dd') , 'Levantador de Pesos', 'MALE');
INSERT INTO PrivateCustomer VALUES(7, 'Ines Gomes', 'gomesi', 'oBastosEhMuitoGiro', 3, Phone_List_Type('915098163'), TO_DATE('1995/06/21', 'yyyy/mm/dd'), 'Barman', 'FEMALE');

/

INSERT INTO Score VALUES(1,1, 1, 9, 'Great place to start the morning');
INSERT INTO Score VALUES(2,2, 5, 7, 'Good stuff');

/

/* SAMPLE QUERIES */

-- List all Points of Interest in Zurich
SELECT * FROM PointOfInterest P
WHERE P.CityId LIKE 4;

 
-- List Telephone Numbers of Contact Júlio Rodrigues
SELECT C.Phone FROM Customer C
WHERE C.CustomerName LIKE 'Julio Rodrigues';

 
-- List all Points of Interest with Category 'Urban'
SELECT P.Name FROM PointOfInterest P, TABLE(P.Category) C
WHERE VALUE (C) LIKE 'Urban';


/* VIEWS */

CREATE OR REPLACE VIEW VCustomer of Customer_Type AS
    SELECT * FROM Customer
/

CREATE OR REPLACE VIEW VPrivateCustomer of PrivateCustomer_Type AS
    SELECT * FROM PrivateCustomer

/

CREATE OR REPLACE VIEW VOrganizationCustomer of OrganizationCustomer_Type AS
    SELECT * FROM OrganizationCustomer
/


/* INHERITANCE SAMPLE QUERIES */

-- List all Organizations with Industry 'IT'
SELECT * FROM VOrganizationCustomer oc WHERE VALUE(oc) IS of(OrganizationCustomer_Type) AND oc.Industry = 'IT';
 

-- List all clients (except organizations) of FEMALE SEX
SELECT * FROM VPrivateCustomer pc WHERE VALUE(pc) IS NOT OF(OrganizationCustomer_Type) AND pc.Gender = 'FEMALE';
 


/* RECURSION */


-- List all Points of Interest starting at root Country 'Switzerland'
SELECT p.Name
FROM PointOfInterest p
INNER JOIN City ct ON ct.CityId = p.CityId
INNER JOIN Country cy ON cy.CountryId = ct.CountryId
START WITH cy.CountryId = 3
CONNECT BY NOCYCLE ct.CountryId = PRIOR cy.CountryId;

/* METHODS */

-- Method to calculate average scoe per Point of Interest

-- DROP TABLES FIRST TO RECREATE NECESSARY TYPES
DROP TABLE SCORE;
/
DROP TABLE PointOfInterest;
/


CREATE OR REPLACE TYPE PointofInterest_Type AS OBJECT (
    PoIId VARCHAR2(30),
    Name VARCHAR2(50),
    Description VARCHAR2(2000),
    CityId VARCHAR2(30),
    Category Category_Type,
    Member Function GetAverageScore Return DECIMAL
);

/

-- Calculate Average Score of a Point of Interest based on all submitted Scores
CREATE OR REPLACE TYPE Body PointOfInterest_Type AS
    Member Function GetAverageScore RETURN DECIMAL IS
    avg_score DECIMAL;
    BEGIN
        SELECT avg(s.Score) INTO avg_score FROM Score s 
        -- WHERE s.PoIId = PoIId;
        RETURN avg_score;
    END;
END;
/


-- Recreate tables and Insert Data
CREATE TABLE PointOfInterest of PointofInterest_Type (
    CONSTRAINT pkPoI PRIMARY KEY(PoIId),
    CONSTRAINT fkPoICity FOREIGN KEY (CityId) REFERENCES City   
  
)
NESTED TABLE Category STORE AS CategoryTable;

/


CREATE TABLE Score of Score_Type (
    CONSTRAINT pkScore PRIMARY KEY (ScoreId),
    CONSTRAINT fkScoreCustomer FOREIGN KEY (CustomerId) REFERENCES Customer,
    CONSTRAINT fkScorePoI FOREIGN KEY (PoIId) REFERENCES PointOfInterest 
)
/


INSERT INTO PointOfInterest VALUES(1, 'Wooden Tower', 'Wooden Tower located in Wil', 4, Category_Type('Rural'));
INSERT INTO PointOfInterest VALUES(2, 'Central Park', 'Central Parks amazing view ', 3, Category_Type('Urban'));
INSERT INTO PointOfInterest VALUES(3, 'Dragon Stadium', 'Host to FC Porto football club', 2, Category_Type('Urban'));
INSERT INTO PointOfInterest VALUES(4, 'Wil Lake', 'Great location in the summer', 4, Category_Type('Water'));
INSERT INTO PointOfInterest VALUES(5, 'Fontelo', 'Relaxing, camping site', 1, Category_Type('Forest'));

/


INSERT INTO Score VALUES(1,1, 1, 9, 'Great place to start the morning');
INSERT INTO Score VALUES(2,2, 5, 7, 'Good stuff');

/