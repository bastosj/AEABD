/* CREATE TYPES */

CREATE OR REPLACE TYPE Company_Type AS TABLE OF VARCHAR(10);
/

CREATE OR REPLACE TYPE Group_Type AS OBJECT (  
   GroupID  VARCHAR2(30),
   GroupName    VARCHAR2(30),
   Company Company_Type
   );
/

CREATE OR REPLACE TYPE Address_Type AS OBJECT (  
   Country  VARCHAR2(30),
   City    VARCHAR2(30),
   Street VARCHAR2(30)
   );
/
CREATE OR REPLACE TYPE Phone_List_Type AS VARRAY(3) of VARCHAR2(10);
/
create or replace TYPE Employee_Type AS OBJECT (  
   FuncNumber  VARCHAR2(30),
   FuncName    VARCHAR2(30),
   Address ADDRESS_TYPE,
   Phone PHONE_LIST_TYPE,
   GrupoId VARCHAR2(30)
   );
/

/* CREATE TABLES */

CREATE TABLE GRUPOS of Group_Type
(
CONSTRAINT primaryGrupoConstraint PRIMARY KEY(GroupID)
)
NESTED TABLE Company STORE AS CompanyTable;
/
CREATE TABLE Employee of Employee_Type
(
CONSTRAINT primaryEmployeeConstraint PRIMARY KEY(FuncNumber),
CONSTRAINT foreignGruposConstraint FOREIGN KEY (GrupoId) REFERENCES Grupos
)
/


/* INSERT DATA */

insert into GRUPOS values(1,'GNorte', COMPANY_TYPE('NorteTec - Tecnologia', 'NorteMob - Mobiliário' , 'NorteTur - Turismo',
'NorteImob - Imobiliária', 'NorteAEC – Arquitetura, Engenharia e Construção'));
/
insert into GRUPOS values(2, 'GSul', COMPANY_TYPE('SulShopping – Centro Comercial', 'SulHealth – Clínica Médica', 'SulGym -
Ginásio', 'SulFashion - Moda', 'SulSports – Material Desportivo', 'SulRetail - Retalho'));
/
insert into EMPLOYEE values(1,'Morgado', ADDRESS_TYPE('PT','Viseu','Direita'),PHONE_LIST_TYPE('960000000','960000001'),1);
/
insert into EMPLOYEE values(2,'Mateus', ADDRESS_TYPE('PT','Espinho','19'),PHONE_LIST_TYPE('960000002','960000003'),1);
/
insert into EMPLOYEE values(3,'Pedroso', ADDRESS_TYPE('ES','Madrid','La Gran Via'),PHONE_LIST_TYPE('960000004','960000005'),2);
/
insert into EMPLOYEE values(4,'Rocha', ADDRESS_TYPE('PT','Braga','R. do Souto'),PHONE_LIST_TYPE('960000006','960000007'),2);
/

/* SAMPLE QUERIES */

/*Em que grupos existem funcionários residentes em Viseu?*/
SELECT GRUPOID FROM EMPLOYEE E
WHERE E.Address.City Like 'Viseu'

 
/* Quais são os números de telefone do funcionário Mateus?*/
SELECT E.Phone FROM EMPLOYEE E
WHERE E.FuncName LIKE 'Mateus'

 


/*A empresa NorteAEC faz parte de que Grupo?*/
SELECT G.GROUPNAME FROM GRUPOS G, TABLE(G.COMPANY) C
WHERE VALUE(C) LIKE '%NorteAEC%'


/* DROP TABLES */ 

DROP TABLE GRUPOS;
/
DROP TABLE EMPLOYEE;
/
DROP TYPE GROUP_TYPE;
/
DROP TYPE EMPLOYEE_TYPE;
/
DROP TYPE COMPANY_TYPE;
/
DROP TYPE PHONE_LIST_TYPE;
/
DROP TYPE ADDRESS_TYPE;
/

/* HERANÇA */

CREATE OR REPLACE TYPE Address_Type AS OBJECT (  
   Country  VARCHAR2(30),
   City    VARCHAR2(400),
   Street VARCHAR2(30)
   );
/
CREATE OR REPLACE TYPE Phone_List_Type AS VARRAY(3) of VARCHAR2(10);
/
Create or Replace Type Person_Type as Object(
    PNumber Varchar2(20),
    PName Varchar2(50),
    Address Address_Type,
    Phone Phone_List_Type
) not final;
/
create or replace TYPE Employee_Type under Person_Type(  
   Salary Varchar2(20),
   Office Varchar2(30),
   Member Function GetAnualSalary Return Varchar2
   );
/
create or replace TYPE Customer_Type under Person_Type(
    NIF Varchar2(30)
   );
/
CREATE TABLE Persons of Person_Type
(
CONSTRAINT primaryPersonConstraint PRIMARY KEY(PNumber)
)
/
CREATE TABLE Employees of Employee_Type
(
CONSTRAINT primaryEmployeeConstraint PRIMARY KEY(PNumber)
)
/
CREATE TABLE Customers of Customer_Type
(
CONSTRAINT primaryCustomerConstraint PRIMARY KEY(PNumber)
)
/
insert into Persons values('1','Person1', Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'));
/
insert into Persons values('2','Person2', Address_Type('Country2','City2','Street2'), PHONE_LIST_TYPE('960000002','960000003'));
/
insert into Persons values('7','Person3', Address_Type('Country2','Viseu','Street2'), PHONE_LIST_TYPE('960000002','960000003'));
/
insert into Employees values('3','Employee1', Address_Type('Country3','City3','Street3'), PHONE_LIST_TYPE('960000004','960000005'),2000,'Office1');
/
insert into Employees values('4','Employee2', Address_Type('Country4','City4','Street4'), PHONE_LIST_TYPE('960000006','960000007'),1500,'Office2');
/
insert into Employees values('8','Employee3', Address_Type('Country4','Viseu','Street4'), PHONE_LIST_TYPE('960000006','960000007'),1500,'Office2');
/
insert into Customers values('5','Customer1', Address_Type('Country5','City5','Street5'), PHONE_LIST_TYPE('960000008','960000009'),000000000);
/
insert into Customers values('6','Customer2', Address_Type('Country6','City6','Street6'), PHONE_LIST_TYPE('960000010','960000011'),000000001);
/
insert into Customers values('9','Customer3', Address_Type('Country6','Viseu','Street6'), PHONE_LIST_TYPE('960000010','960000011'),000000001);
/

/* Criar método de cálculo de salário */

Create or Replace Type Body Employee_Type AS
    Member Function GetAnualSalary Return Varchar2 IS
    Begin
        return salary*12;
    END;
END;
/

2.3

Create or Replace View VPersons of Person_Type as 
    Select * From Persons;
/
Create or Replace View VEmployees of Employee_Type under VPersons as
    Select * From Employees;
/
Create Or Replace View VCustomers of Customer_Type under VPersons as
    Select * From Customers;
/

a)	Todas as pessoas
select * from VPersons p where Value(p) is of(Person_Type) and p.Address.City = 'Viseu';
/
 

b)	Pessoas que não são clientes nem funcionários
select * from VPersons p where Value(p) is not of(Employee_Type) and Value(p) is not of(Customer_Type) and p.Address.City = 'Viseu';
/
 

c)	Funcionários
select p.*, treat(Value(p) as Employee_Type).Office as Office, treat(Value(p) as Employee_Type).Salary as Salary from VPersons p where Value(p) is of(Employee_Type) and p.Address.City = 'Viseu';
/
 

d)	Método salário_anual para mostrar o salário anual de cada funcionário
select treat(Value(p) as Employee_Type).PName, treat(Value(p) as Employee_Type).GetAnualSalary() from VPersons p where Value(p) is of(Employee_Type);
/
 

2.4

Create Table PersonH of Person_Type
(
CONSTRAINT primaryPersonHConstraint PRIMARY KEY(PNumber)
)
/
insert into PersonH Values(Person_Type('1','Person1', Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001')));
/
insert into PersonH values(Person_Type('2','Person2', Address_Type('Country2','City2','Street2'), PHONE_LIST_TYPE('960000002','960000003')));
/
insert into PersonH values(Person_Type('7','Person3', Address_Type('Country2','Viseu','Street2'), PHONE_LIST_TYPE('960000002','960000003')));
/
insert into PersonH values(Employee_Type('3','Employee1', Address_Type('Country3','City3','Street3'), PHONE_LIST_TYPE('960000004','960000005'),2000,'Office1'));
/
insert into PersonH values(Employee_Type('4','Employee2', Address_Type('Country4','City4','Street4'), PHONE_LIST_TYPE('960000006','960000007'),1500,'Office2'));
/
insert into PersonH values(Employee_Type('8','Employee3', Address_Type('Country4','Viseu','Street4'), PHONE_LIST_TYPE('960000006','960000007'),1500,'Office2'));
/
insert into PersonH values(Customer_Type('5','Customer1', Address_Type('Country5','City5','Street5'), PHONE_LIST_TYPE('960000008','960000009'),000000000));
/
insert into PersonH values(Customer_Type('6','Customer2', Address_Type('Country6','City6','Street6'), PHONE_LIST_TYPE('960000010','960000011'),000000001));
/
insert into PersonH values(Customer_Type('9','Customer3', Address_Type('Country6','Viseu','Street6'), PHONE_LIST_TYPE('960000010','960000011'),000000001));
/

a)
select * from PersonH p where Value(p) is of(Person_Type);
/
 

b)
select * from PersonH p where Value(p) is not of(Employee_Type) and Value(p) is not of(Customer_Type);
/
 

c)
select p.*, treat(Value(p) as Employee_Type).Office as Office, treat(Value(p) as Employee_Type).Salary as Salary from PersonH p where Value(p) is of(Employee_Type);
/
 

d)
select p.*, treat(Value(p) as Customer_Type).NIF as NIF from PersonH p where Value(p) is of(Customer_Type);
/
 

2.5

Drop Table PersonH;
/
Drop Table Persons;
/
Drop Table Customers;
/
Drop Table Employees;
/
Drop View VCustomers;
/
Drop View VEmployees;
/
Drop View VPersons;
/
Drop Type Customer_Type;
/
Drop Type Employee_Type;
/
Drop Type Person_Type;
/
Drop Type Phone_List_Type;
/
Drop Type Address_Type;
/

/* Recursividade */

a)
CREATE OR REPLACE TYPE Address_Type AS OBJECT (  
   Country  VARCHAR2(30),
   City    VARCHAR2(400),
   Street VARCHAR2(30)
   );
/
CREATE OR REPLACE TYPE Phone_List_Type AS VARRAY(3) of VARCHAR2(10);
/
Create or Replace Type Person_Type as Object(
    PNumber Varchar2(20),
    PName Varchar2(50),
    Address Address_Type,
    Phone Phone_List_Type
) not final;
/
create or replace TYPE Employee_Type under Person_Type(  
   Salary Varchar2(20),
   Office Varchar2(30),
   Boss ref Employee_Type
   );
/
CREATE TABLE Employees of Employee_Type
(
CONSTRAINT primaryEmployeeConstraint PRIMARY KEY(PNumber),
Boss scope is Employees
)
/

b)
Insert into Employees values(1, 'João',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', null);
/
Insert into Employees values(2, 'Maria',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', (Select ref(e) from Employees e where e.PNAME = 'João'));
/
Insert into Employees values(3, 'Pedro',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', (Select ref(e) from Employees e where e.PNAME = 'João'));
/
Insert into Employees values(4, 'Felix',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', (Select ref(e) from Employees e where e.PNAME = 'Pedro'));
/
Insert into Employees values(5, 'Eva',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', (Select ref(e) from Employees e where e.PNAME = 'Pedro'));
/
Insert into Employees values(6, 'Carlos',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', (Select ref(e) from Employees e where e.PNAME = 'Maria'));
/
Insert into Employees values(7, 'Paulo',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', null);
/
Insert into Employees values(8, 'Jorge',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', (Select ref(e) from Employees e where e.PNAME = 'Paulo'));
/
Insert into Employees values(9, 'Claudio',Address_Type('Country1','City1','Street1'), PHONE_LIST_TYPE('960000000','960000001'), '2500', 'Office1', (Select ref(e) from Employees e where e.PNAME = 'Paulo'));
/

3.2

a)
SELECT PName, DEREF(Boss).PName from Employees;
/
SELECT e.PName, e.Boss.PName from Employees e;
/ 

b)
SELECT e.PName
FROM Employees e
START WITH part = 'João'
CONNECT BY PRIOR e.PName = e.Boss.PName;
