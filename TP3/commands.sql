
-- CREATE SCHEMA

-- Sccsid:     @(#)dss.ddl	2.1.8.1
CREATE TABLE NATION  ( N_NATIONKEY  INTEGER NOT NULL,
                            N_NAME       CHAR(25) NOT NULL,
                            N_REGIONKEY  INTEGER NOT NULL,
                            N_COMMENT    VARCHAR(152));

CREATE TABLE REGION  ( R_REGIONKEY  INTEGER NOT NULL,
                            R_NAME       CHAR(25) NOT NULL,
                            R_COMMENT    VARCHAR(152));

CREATE TABLE PART  ( P_PARTKEY     INTEGER NOT NULL,
                          P_NAME        VARCHAR(55) NOT NULL,
                          P_MFGR        CHAR(25) NOT NULL,
                          P_BRAND       CHAR(10) NOT NULL,
                          P_TYPE        VARCHAR(25) NOT NULL,
                          P_SIZE        INTEGER NOT NULL,
                          P_CONTAINER   CHAR(10) NOT NULL,
                          P_RETAILPRICE DECIMAL(15,2) NOT NULL,
                          P_COMMENT     VARCHAR(23) NOT NULL );

CREATE TABLE SUPPLIER ( S_SUPPKEY     INTEGER NOT NULL,
                             S_NAME        CHAR(25) NOT NULL,
                             S_ADDRESS     VARCHAR(40) NOT NULL,
                             S_NATIONKEY   INTEGER NOT NULL,
                             S_PHONE       CHAR(15) NOT NULL,
                             S_ACCTBAL     DECIMAL(15,2) NOT NULL,
                             S_COMMENT     VARCHAR(101) NOT NULL);

CREATE TABLE PARTSUPP ( PS_PARTKEY     INTEGER NOT NULL,
                             PS_SUPPKEY     INTEGER NOT NULL,
                             PS_AVAILQTY    INTEGER NOT NULL,
                             PS_SUPPLYCOST  DECIMAL(15,2)  NOT NULL,
                             PS_COMMENT     VARCHAR(199) NOT NULL );

CREATE TABLE CUSTOMER ( C_CUSTKEY     INTEGER NOT NULL,
                             C_NAME        VARCHAR(25) NOT NULL,
                             C_ADDRESS     VARCHAR(40) NOT NULL,
                             C_NATIONKEY   INTEGER NOT NULL,
                             C_PHONE       CHAR(15) NOT NULL,
                             C_ACCTBAL     DECIMAL(15,2)   NOT NULL,
                             C_MKTSEGMENT  CHAR(10) NOT NULL,
                             C_COMMENT     VARCHAR(117) NOT NULL);

CREATE TABLE ORDERS  ( O_ORDERKEY       INTEGER NOT NULL,
                           O_CUSTKEY        INTEGER NOT NULL,
                           O_ORDERSTATUS    CHAR(1) NOT NULL,
                           O_TOTALPRICE     DECIMAL(15,2) NOT NULL,
                           O_ORDERDATE      DATE NOT NULL,
                           O_ORDERPRIORITY  CHAR(15) NOT NULL,  
                           O_CLERK          CHAR(15) NOT NULL, 
                           O_SHIPPRIORITY   INTEGER NOT NULL,
                           O_COMMENT        VARCHAR(79) NOT NULL);

CREATE TABLE LINEITEM ( L_ORDERKEY    INTEGER NOT NULL,
                             L_PARTKEY     INTEGER NOT NULL,
                             L_SUPPKEY     INTEGER NOT NULL,
                             L_LINENUMBER  INTEGER NOT NULL,
                             L_QUANTITY    DECIMAL(15,2) NOT NULL,
                             L_EXTENDEDPRICE  DECIMAL(15,2) NOT NULL,
                             L_DISCOUNT    DECIMAL(15,2) NOT NULL,
                             L_TAX         DECIMAL(15,2) NOT NULL,
                             L_RETURNFLAG  CHAR(1) NOT NULL,
                             L_LINESTATUS  CHAR(1) NOT NULL,
                             L_SHIPDATE    DATE NOT NULL,
                             L_COMMITDATE  DATE NOT NULL,
                             L_RECEIPTDATE DATE NOT NULL,
                             L_SHIPINSTRUCT CHAR(25) NOT NULL,
                             L_SHIPMODE     CHAR(10) NOT NULL,
                             L_COMMENT      VARCHAR(44) NOT NULL);


-- CLEANSE DATA
-- remove last comma on each line of CSV

bash
cd /mnt/c/Users/bastosj/Desktop/AEABD/TP3/stuff/tpch8/csv/;
sed -i 's/,$//' nation.csv;
sed -i 's/,$//' region.csv;
sed -i 's/,$//' customer.csv;
sed -i 's/,$//' part.csv;
sed -i 's/,$//' partsupp.csv;
sed -i 's/,$//' supplier.csv;
sed -i 's/,$//' orders.csv;
sed -i 's/,$//' lineitem.csv;


-- IMPORT DATA
\COPY nation (N_NATIONKEY, N_NAME, N_REGIONKEY, N_COMMENT) FROM 'C:\Users\bastosj\Desktop\AEABD\TP3\stuff\tpch8\csv\nation.csv' WITH (FORMAT csv);

\COPY region (R_REGIONKEY, R_NAME, R_COMMENT) FROM 'C:\Users\bastosj\Desktop\AEABD\TP3\stuff\tpch8\csv\region.csv' WITH (FORMAT csv);

\COPY customer (C_CUSTKEY, C_NAME, C_ADDRESS, C_NATIONKEY, C_PHONE, C_ACCTBAL, C_MKTSEGMENT, C_COMMENT) FROM 'C:\Users\bastosj\Desktop\AEABD\TP3\stuff\tpch8\csv\customer.csv' WITH (FORMAT csv);

\COPY part (P_PARTKEY,  P_NAME, P_MFGR, P_BRAND, P_TYPE, P_SIZE, P_CONTAINER, P_RETAILPRICE, P_COMMENT) FROM 'C:\Users\bastosj\Desktop\AEABD\TP3\stuff\tpch8\csv\part.csv' WITH (FORMAT csv);

\COPY partsupp (PS_PARTKEY, PS_SUPPKEY, PS_AVAILQTY, PS_SUPPLYCOST, PS_COMMENT) FROM 'C:\Users\bastosj\Desktop\AEABD\TP3\stuff\tpch8\csv\partsupp.csv' WITH (FORMAT csv);

\COPY supplier (S_SUPPKEY, S_NAME, S_ADDRESS, S_NATIONKEY, S_PHONE, S_ACCTBAL, S_COMMENT) FROM 'C:\Users\bastosj\Desktop\AEABD\TP3\stuff\tpch8\csv\supplier.csv' WITH (FORMAT csv);

\COPY orders (O_ORDERKEY, O_CUSTKEY, O_ORDERSTATUS, O_TOTALPRICE, O_ORDERDATE, O_ORDERPRIORITY, O_CLERK, O_SHIPPRIORITY, O_COMMENT) FROM 'C:\Users\bastosj\Desktop\AEABD\TP3\stuff\tpch8\csv\orders.csv' WITH (FORMAT csv);

\COPY lineitem (L_ORDERKEY, L_PARTKEY, L_SUPPKEY, L_LINENUMBER, L_QUANTITY, L_EXTENDEDPRICE, L_DISCOUNT, L_TAX, L_RETURNFLAG, L_LINESTATUS, L_SHIPDATE, L_COMMITDATE, L_RECEIPTDATE, L_SHIPINSTRUCT, L_SHIPMODE, L_COMMENT) FROM 'C:\Users\bastosj\Desktop\AEABD\TP3\stuff\tpch8\csv\lineitem.csv' WITH (FORMAT csv);


/* QUERIES */

-- A
EXPLAIN ANALYZE SELECT COUNT (*) FROM lineitem;

-- B
EXPLAIN ANALYZE SELECT o_orderkey , n1.n_name , n2. n_name FROM orders , lineitem , partsupp , supplier , nation n1 , nation n2 , customer WHERE o_orderkey = l_orderkey AND l_partkey = ps_partkey AND l_suppkey = s_suppkey AND s_nationkey = n1. n_nationkey AND o_custkey = c_custkey AND c_nationkey = n2. n_nationkey AND o_orderkey = 492164;

--C

EXPLAIN ANALYZE SELECT l_orderkey , l_linenumber , l_extendedprice FROM lineitem WHERE l_extendedprice < 1000;

--D

EXPLAIN ANALYZE SELECT l_orderkey , l_linenumber , l_extendedprice FROM lineitem WHERE l_extendedprice < 100000;

--E

EXPLAIN ANALYZE SELECT l_orderkey , l_partkey FROM lineitem , part WHERE l_partkey = p_partkey AND p_size = 20 AND p_container = 'JUMBO BOX' AND p_type = 'PROMO BURNISHED COPPER';


-- F
EXPLAIN ANALYZE SELECT c_custkey, c_name FROM Customer WHERE LOWER (c_name) = 'customer#000000010';


-- CREATE KEYS

-- ALTER TABLE REGION DROP CONSTRAINT R_REGIONKEY;
-- ALTER TABLE NATION DROP CONSTRAINT N_NATIONKEY;
-- ALTER TABLE PART DROP CONSTRAINT P_PARTKEY;
-- ALTER TABLE SUPPLIER DROP CONSTRAINT S_SUPPKEY;
-- ALTER TABLE PARTSUPP DROP CONSTRAINT KEY;
-- ALTER TABLE ORDERS DROP CONSTRAINT KEY;
-- ALTER TABLE LINEITEM DROP CONSTRAINT KEY;
-- ALTER TABLE CUSTOMER DROP CONSTRAINT KEY;


ALTER TABLE REGION ADD PRIMARY KEY (R_REGIONKEY);

ALTER TABLE NATION ADD PRIMARY KEY (N_NATIONKEY);

ALTER TABLE NATION ADD FOREIGN KEY NATION_FK1 (N_REGIONKEY) references REGION;

ALTER TABLE PART ADD PRIMARY KEY (P_PARTKEY);

ALTER TABLE SUPPLIER ADD PRIMARY KEY (S_SUPPKEY);

ALTER TABLE SUPPLIER ADD FOREIGN KEY SUPPLIER_FK1 (S_NATIONKEY) references NATION;

ALTER TABLE PARTSUPP ADD PRIMARY KEY (PS_PARTKEY,PS_SUPPKEY);

ALTER TABLE CUSTOMER ADD PRIMARY KEY (C_CUSTKEY);

ALTER TABLE CUSTOMER ADD FOREIGN KEY CUSTOMER_FK1 (C_NATIONKEY) references NATION;

ALTER TABLE LINEITEM ADD PRIMARY KEY (L_ORDERKEY,L_LINENUMBER);

ALTER TABLE ORDERS ADD PRIMARY KEY (O_ORDERKEY);

ALTER TABLE PARTSUPP ADD FOREIGN KEY PARTSUPP_FK1 (PS_SUPPKEY) references SUPPLIER;

ALTER TABLE PARTSUPP ADD FOREIGN KEY PARTSUPP_FK2 (PS_PARTKEY) references PART;

ALTER TABLE ORDERS ADD FOREIGN KEY ORDERS_FK1 (O_CUSTKEY) references CUSTOMER;

ALTER TABLE LINEITEM ADD FOREIGN KEY LINEITEM_FK1 (L_ORDERKEY)  references ORDERS;

ALTER TABLE LINEITEM ADD FOREIGN KEY LINEITEM_FK2 (L_PARTKEY,L_SUPPKEY) references PARTSUPP;




/* CREATE INDEXES */

CREATE INDEX l_extendedprice_idx ON lineitem (l_extendedprice);

CREATE INDEX p_part_idx ON part (p_size, p_container, p_type);

DROP INDEX l_extendedprice_idx;
DROP INDEX p_part_idx;


/* DROP TABLES */

DROP TABLE REGION;
DROP TABLE NATION;
DROP TABLE PART;
DROP TABLE SUPPLIER;
DROP TABLE PARTSUPP;
DROP TABLE ORDERS;
DROP TABLE LINEITEM;
DROP TABLE CUSTOMER;