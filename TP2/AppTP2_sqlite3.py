import sqlite3
from sqlite3 import Error

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return None

def select_all_regions(conn):
    """
    Query all rows in the regions table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM region")
 
    rows = cur.fetchall()
 
    for row in rows:
        print(row)

def create_region(conn, project):
    """
    Create a new project into the regions table
    :param conn:
    :param project:
    :return: project id
    """
    sql = ''' INSERT INTO region(regionkey,name,comment)
              VALUES(?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, project)
    return cur.lastrowid

def main():
    # create a database connection
    database = 'C:\sqlite\db\\tp2.db'
    conn = create_connection(database)
    command = input("GetData or SetData \n")
    with conn:
        if command == "GetData":
            # Query all regions
            print("2. Query all Regions")
            select_all_regions(conn)
        elif command == "SetData":
            # create a new region
            region = (5, 'MOON', 'The moon is shiny!')
            region_id = create_region(conn, region)
            print("Created "+ region_id)
        else:
            print("Operation not available")

if __name__ == '__main__':
    main()