import redis

def connectToRedis():
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    r.set('foo', 'bar')
    foo = r.get('foo')
    print(foo)

def main():
    # print("Hello World")
    connectToRedis()



if __name__ == '__main__':
    main()